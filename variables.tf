variable "access_key" {
  description = "administrator username"
  type        = string
  sensitive   = true
}

variable "secret_key" {
description = "administrator password"
  type        = string
  sensitive   = true
}